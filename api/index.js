require('dotenv').config()
let process = require('process')
const express = require('express')
let exeAddr = process.env.EXECUTIONER_ADDRESS
let axios = require('axios')
// Create express router
const router = express.Router()
const bodyParser = require('body-parser')

// Transform req & res to have the same API as express
// So we can use res.status() & res.json()
var app = express()
app.set('trust proxy', 'loopback')
router.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request)
  Object.setPrototypeOf(res, app.response)
  req.res = res
  res.req = req
  next()
})
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

let https = require('https')
let httpCfg = {
  proxy: false,
  headers: { 'Content-Type': 'application/json' }
}
if (process.env.EXECUTIONER_SSL_ENABLED === 'true') {
  httpCfg.httpsAgent = new https.Agent({
    rejectUnauthorized: false
  })
}

// Add POST - /api/login
router.post('/login', (req, resp) => {
  let info = JSON.stringify({
    account: req.body.account,
    password: req.body.password,
    type: req.body.type
  }) // Do not use req.body directly since JSON has circular structure
  axios.post(`${exeAddr}/api/v1/login`, info, httpCfg)
    .then((response) => {
      req.session.user = {
        uid: response.data.uid,
        role: response.data.role,
        name: response.data.name
      }

      // Also write backend cookie
      if (response.headers['set-cookie']) {
        resp.set('Set-Cookie', response.headers['set-cookie'])
      }
      resp.status(200).json(response.data)
      resp.end()
    })
    .catch((err) => {
      if (err.response) {
        resp
          .status(err.response.status)
          .json(err.message)
      } else if (err.request) {
        resp
          .status(500)
          .json('Internal server error')
        console.error(err.request)
      } else {
        resp
          .status(500)
          .json('Internal server error!')
        console.log('Error', err.message)
      }
      resp.end()
    })
})

// Add POST - /api/logout
router.post('/logout', (req, res) => {
  req.session.user = {}
  req.session.destroy()
  res.clearCookie('cf_fe_session', { maxAge: 0, path: '/', domain: '.codefighter.app' })

  // Call backend logout
  axios.post(`${exeAddr}/api/v1/logout`, null, httpCfg)
    .then((resp) => {})
    .catch((err) => {
      console.log('backend logout error', err)
    })

  res.status(200).json({ ok: true })
  res.end()
})

// Export the server middleware
module.exports = {
  path: '/api',
  handler: router
}
