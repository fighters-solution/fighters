import process from 'process'
import Vue from 'vue'
import moment from 'moment'
import dotenv from 'dotenv'
dotenv.config()

export const role = {
  Administrator: 1,
  Moderator: 5,
  NormalUser: 9,
  Anonymous: 13
}

export const time = {
  now () {
    return +moment() / 1000 // To Unix timestamp in second
  }
}

export const eventBus = new Vue({
  methods: {
    snackbarShown (data) {
      this.$emit('showSnackbar', data)
    },
    loginModalShown () {
      this.$emit('showLoginModal')
    },
    userLoggedIn () {
      this.$emit('loginUser')
    },
    logoutModalShown () {
      this.$emit('showLogoutModal')
    },
    courseModalShown (items) {
      this.$emit('showCourseModal', items)
    },
    userLoggedOut () {
      this.$emit('logoutUser')
    },
    challengeSolved () {
      this.$emit('solveChallenge')
    },
    challengeVersionsModalShown () {
      this.$emit('showChallengeVersionsModal')
    },
    challengeLeaderboardModalShown () {
      this.$emit('showChallengeLeaderboardModal')
    },
    versionCodeChanged (version) {
      this.$emit('changeVersionCode', version)
    },
    userProfileEditModalShown (user) {
      this.$emit('showUserProfileEditModal', user)
    },
    userProfileUpdated (user) {
      this.$emit('updateUserProfile', user)
    },
    settingsModalShown () {
      this.$emit('showSettingsModal')
    },
    drawerToggled (val) {
      this.$emit('forceToggleDrawer', val)
    },
    editChallengeModalToggled (cid) {
      this.$emit('toggleEditChallengeModal', cid)
    },
    testCasesEditorModalShown (tmpl, data) {
      this.$emit('showTestCasesEditorModal', tmpl, data)
    },
    testCasesDataChanged (data) {
      this.$emit('changeTestCasesData', data)
    },
    challengeJsonModalShown (data) {
      this.$emit('showChallengeJsonModal', data)
    },
    emailConfimed () {
      this.$emit('confirmEmail')
    },
    notificationModalShown () {
      this.$emit('showNotificationModal')
    },
    notificationsChanged () {
      this.$emit('changeNotifications')
    },
    congratsModalShown (data) {
      this.$emit('showCongratsModal', data)
    }
  }
})

let https = require('https')
let cfg = {
  proxy: false,
  headers: { 'Content-Type': 'application/json' }
}
if (process.env.EXECUTIONER_SSL_ENABLED === 'true') {
  cfg.httpsAgent = new https.Agent({
    rejectUnauthorized: false
  })
}
export const httpCfg = cfg

let levels = [
  { idx: 1, text: 'Veteran', xp: 0, step: 100 },
  { idx: 2, text: 'Swordsman', xp: 500, step: 200 },
  { idx: 3, text: 'Warrior', xp: 1500, step: 300 },
  { idx: 4, text: 'Champion', xp: 3000, step: 400 },
  { idx: 5, text: 'Hero', xp: 5000, step: 500 },
  { idx: 6, text: 'Legend', xp: 7500, step: 600 },
  { idx: 7, text: 'God of War', xp: 10500, step: 1000 }
]

function romanize (num) {
  if (!+num) {
    return ''
  }
  let digits = String(+num).split('')
  let key = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM',
    '', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC',
    '', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX']
  let roman = ''
  let i = 3
  while (i--) {
    roman = (key[+digits.pop() + (i * 10)] || '') + roman
  }
  return Array(+digits.join('') + 1).join('M') + roman
}

function getLevel (xp) {
  for (let i = levels.length - 1; i >= 0; i--) {
    if (xp >= levels[i].xp) {
      return levels[i]
    }
  }
  return levels[0]
}

function getStepLevel (lvl, xp) {
  let stepXp = xp - lvl.xp
  let step = Math.floor(stepXp / lvl.step)
  return step + 1
}

export const level = {
  getTextLevel (xp) {
    if (xp === 0) {
      return levels[0].text + ' I'
    }
    let lvl = getLevel(xp)
    return lvl.text + ' ' + romanize(getStepLevel(lvl, xp))
  },
  getPercLevel (xp) {
    if (xp === 0) {
      return 0
    }
    let lvl = getLevel(xp)
    return (this.getEarnedXPLevel(xp) / lvl.step) * 100
  },
  getEarnedXPLevel (xp) {
    if (xp === 0) {
      return 0
    }
    let lvl = getLevel(xp)
    let step = getStepLevel(lvl, xp)
    let stepXp = lvl.xp + (step - 1) * lvl.step
    return xp - stepXp
  },
  getNeededXPLevel (xp) {
    let lvl = getLevel(xp)
    return lvl.step
  },
  getLevelIcon (xp) {
    if (xp === 0) {
      return '/ranks/1-1.png'
    }
    let lvl = getLevel(xp)
    let step = getStepLevel(lvl, xp)

    if (lvl.idx === 7) { // Last level
      if (step >= 15) {
        return `/ranks/top1.png`
      }
      if (step >= 10) {
        return `ranks/top2.png`
      }
      if (step > 5) {
        return `ranks/top3.png`
      }
    }
    return `/ranks/${lvl.idx}-${step}.png`
  }
}

const levelUpTexts = [
  'Congratulations for reaching new level. Keep up your good works. You\'re doing well!',
  'Tránh đường. I\'m on my way to become the legend',
  'Who can stop me now?',
  'Bring me new challenges. Those are too easy',
  'People said that noone ever reached this level... before me',
  'Legend said that noone can defeat those monsters... until they meet me',
  'I\'m not so good but more than enough to beat this level',
  'I\'m the second strongest hero in the world. The strongest one is noone.',
  'They said noone can beat this level. My name is Noone'
]
let levelUpTextsTmp = JSON.parse(JSON.stringify(levelUpTexts))

let solvedTexts = [
  'Keep up your good works. You\'re doing well!',
  'Tránh đường. I\'m on my way to become a legend',
  'Only one can stop me now. It\'s noone',
  'Come on. This challenge is too easy for me',
  'Bring me the harder ones!!!',
  'Easy peeeeeeasy. I solved this challenge in 5 seconds',
  'Easy peasy lemon squeezy. I didn\'t even think when solving this challenge',
  'First blood. And it\'s just the beginning',
  'Double kill!',
  'Triple kill!',
  'Quadra kill!',
  'Penta kill!',
  'Even rampage cannot satisfy me now',
  'Godlike! Somebody stop me, please',
  'Ba dum tsss! Think again before challenging me',
  'Legend said that noone can defeat this challenge... except me',
  'I\'m the chosen one',
  'They said noone can defeat this challenge. My name is Noone'
]
let solvedTextsTmp = JSON.parse(JSON.stringify(solvedTexts))

export const congratsText = {
  getLevelUpText () {
    if (levelUpTextsTmp.length === 0) {
      levelUpTextsTmp = JSON.parse(JSON.stringify(levelUpTexts))
    }
    let idx = Math.floor(Math.random() * Math.floor(levelUpTextsTmp.length))
    let text = levelUpTextsTmp[idx]
    levelUpTextsTmp.splice(idx, 1) // Remove current item so it won't be appeared again at next random
    return text
  },
  getSolvedText () {
    if (solvedTextsTmp.length === 0) {
      solvedTextsTmp = JSON.parse(JSON.stringify(solvedTexts))
    }
    let idx = Math.floor(Math.random() * Math.floor(solvedTextsTmp.length))
    let text = solvedTextsTmp[idx]
    solvedTextsTmp.splice(idx, 1) // Remove current item so it won't be appeared again at next random
    return text
  }
}
