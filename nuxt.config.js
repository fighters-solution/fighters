// TODO we should remove this when running in production mode - and use real
//      environment variables
import dotenv from 'dotenv'
// For middleware
import process from 'process'
import bodyParser from 'body-parser'
import session from 'express-session'

var redisStore = require('connect-redis')(session)

dotenv.config()

// console.log(process.env)

export default {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Code Fighter',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'An online coding arena' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico?v=2' },
      { rel: 'stylesheet', href: '/style.css' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
    ]
  },
  layoutTransition: {
    name: 'fadeLayout',
    mode: 'out-in'
  },
  modules: [
    '@nuxtjs/dotenv', '@nuxtjs/axios'
  ],
  plugins: [
    // 'babel-polyfill', // Disable since causing 'only one instance of babel-polyfill is allowed' in development
    '~/plugins/vuetify.js',
    {
      src: '~/plugins/vue-bulma-brace',
      ssr: false
    },
    '~/plugins/utilities.js'
  ],
  css: [
    '~/assets/style/app.styl'
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    // vendor: [
    //   '~/plugins/vuetify.js'
    // ],
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      config.node = {
        fs: 'empty'
      }
      if (ctx.isDev && process.client) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    maxChunkSize: 300000,
    splitChunks: {
      layouts: true
    }
  },
  serverMiddleware: [
    // body-parser middleware
    bodyParser.json(),
    // session middleware
    session({
      name: process.env.COOKIE_NAME,
      secret: process.env.SESSION_SECRET,
      saveUninitialized: false,
      resave: false,
      rolling: false,
      cookie: {
        domain: process.env.SESSION_DOMAIN,
        path: process.env.SESSION_PATH,
        maxAge: process.env.SESSION_LIFETIME_DAY * 24 * 3600 * 1000,
        httpOnly: process.env.SESSION_HTTP_ONLY === 'true',
        secure: process.env.SESSION_SECURE === 'true'
      },
      store: new redisStore({
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        pass: process.env.REDIS_PASSWORD
      })
    }),
    // // Api middleware
    // // We add /api/login & /api/logout routes
    '~/api'
  ]
}
