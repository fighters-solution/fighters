import Vue from 'vue'
import VueLocalStorage from 'vue-ls'
import md from 'marked'
import moment from 'moment'
let emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

Vue.use(VueLocalStorage)

md.setOptions({
  renderer: new md.Renderer(),
  gfm: true,
  tables: true,
  breaks: true,
  pedantic: false,
  sanitize: false,
  smartLists: true,
  smartypants: false
})

Vue.mixin({
  filters: {
    unixTsToRelativeTime (val) {
      return moment(val * 1000).fromNow() // Unix ts in sec -> ms
    },
    unixTsToUTC (val) {
      return moment(val * 1000).utc().format()
    },
    toAvatarURL (val) {
      return val ? (val.startsWith('/api/v1/users/') ? val : `/api/v1/users/${val}/avatar`)
        : `/api/v1/users/null/avatar`
    },
    toPrettyJSON (val) {
      return JSON.stringify(val, null, 2)
    },
    toFixed4FPs (val) {
      return +parseFloat(val).toFixed(4)
    },
    upperFirstChar (val) {
      return val.charAt(0).toUpperCase() + val.slice(1)
    }
  },
  methods: {
    marked: str => md(str),
    langIcon: (lan) => {
      let l = lan || 'code'
      if (l) {
        l = l.toLowerCase()
        let trans
        switch (l) {
          case 'go':
            trans = 'go-lang'
            break
          case 'python3':
            trans = 'python'
            break
          case 'c':
            trans = 'c-lang'
            break
          case 'cpp':
            trans = 'cplusplus'
            break
          default:
            trans = l
        }
        return `rye-${trans}`
      }
      return null
    },
    cloneObject (obj) {
      return JSON.parse(JSON.stringify(obj))
    },
    isValidEmail (email) {
      return emailRegex.test(email)
    },
    getCookie (name) {
      var value = '; ' + document.cookie
      var parts = value.split('; ' + name + '=')
      if (parts.length === 2) {
        return parts.pop().split(';').shift()
      }
    },
    deleteCookie (name, domain, path) {
      if (!domain) {
        domain = window.location.hostname
      }
      if (!path) {
        path = '/'
      }
      document.cookie = `${name}=; Max-Age=0`
      document.cookie = `${name}=; Path=${path}; Domain=${domain}; Max-Age=0`
    }
  }
})
